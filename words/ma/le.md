# male<sup>1</sup>

American-phonetic:   meyl

---

## adjective



1     having or relating to a gender identity that corresponds to a complex, variable set of social and cultural roles, traits, and behaviors assigned to people of the sex that typically produces sperm cells. gender1

2 a   Biology. of, relating to, or being an animal or human of the sex or sexual phase that normally produces sperm cells during reproduction. sex1

  b   Botany. designating or relating to a plant or its reproductive structure producing or containing microspores.

  c   Botany. (of seed plants) staminate.

  d   of, relating to, or characteristic of a male person; masculine:

a male voice.



comprising male people:

a male choir.



Machinery. made to fit into a corresponding open or recessed part: Compare female ( def 5 ).

a male plug.