# equity

American-phonetic:   ek-wi-tee

**noun** *plural **eq-ui-ties.***

---



1   the quality of being fair or impartial; <a href="https://www.dictionary.com/browse/fairness">fairness</a>; <a href="https://www.dictionary.com/browse/impartiality">impartiality</a>:

>   the equity of Solomon

**Synonyms:** <a href="https://www.thesaurus.com/browse/justice">justice</a>, <a href="https://www.thesaurus.com/browse/objectivity">objectivity</a>, <a href="https://www.thesaurus.com/browse/justness">justness</a>, <a href="https://www.thesaurus.com/browse/disinterest">disinterest</a>

**Antonyms:** <a href="https://www.thesaurus.com/browse/discrimination">discrimination</a>, <a href="https://www.thesaurus.com/browse/bias">bias</a>, <a href="https://www.thesaurus.com/browse/injustice">injustice</a>, prejudice, partisanship, partiality, inequity



2   something that is fair and just:

>   the concepts and principles of health equities and inequities are important to society as a whole.  



3   the policy or practice of accounting for the differences in each individual's starting point when pursuing a goal or achievement, and working to remove barriers to equal opportunity, as by providing support based on the unique needs of individual students or employees.  Compare equility (def 1).  
>